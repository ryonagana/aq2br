# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('text', models.TextField()),
                ('pub_date', models.DateTimeField(verbose_name='publication date')),
                ('image', models.CharField(null=True, max_length=80, blank=True)),
                ('headline', models.BooleanField(default=False)),
                ('approved', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('username', models.CharField(max_length=20)),
                ('user_passwd', models.CharField(max_length=64)),
                ('email', models.CharField(max_length=127)),
                ('block', models.BooleanField(default=True)),
                ('name', models.CharField(max_length=65)),
                ('surname', models.CharField(max_length=127)),
                ('sex', models.CharField(max_length=1, default='M', choices=[('M', 'Masculino'), ('F', 'Feminino')])),
            ],
        ),
        migrations.AddField(
            model_name='article',
            name='user',
            field=models.ForeignKey(to='article.User'),
        ),
    ]
