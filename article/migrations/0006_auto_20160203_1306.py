# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-03 15:06
from __future__ import unicode_literals

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0005_auto_20160203_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='image',
            field=models.FileField(blank=True, storage=django.core.files.storage.FileSystemStorage(location='D:\\Xampp\\htdocs\\testes\\python\\aq2br\\aq2br\\static\\images'), upload_to=''),
        ),
    ]
