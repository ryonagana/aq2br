from django.db import models
from django.template.defaultfilters import slugify
from django.core.files.storage import FileSystemStorage
from django.conf import settings
# Create your models here.

from file.models import File

MASCULINE='M'
FEMININE='F'

SEX_CHOICE = (
    (MASCULINE, 'Masculino'),
    (FEMININE, 'Feminino')
)


image_storage  = FileSystemStorage(location= settings.FILE_PATH_FIELD_DIRECTORY)



class User(models.Model):
    username = models.CharField(max_length=20)
    user_passwd = models.CharField(max_length=64)
    email = models.CharField(max_length=127)
    block = models.BooleanField(default=True)
    name = models.CharField(max_length=65)
    nickname = models.CharField(max_length=35, blank=True)
    surname = models.CharField(max_length=127)
    clan_tag = models.CharField(max_length=6,  blank=True)
    favorite_game = models.ForeignKey('file.Game', blank=True, default=False)


    sex = models.CharField(max_length=1, choices=SEX_CHOICE, default=MASCULINE)




    def listAllClans(self):
        clans= User.objects.order_by('clan_tag').distinct()
        return clans



class Article(models.Model):
        title = models.CharField(max_length=100)
        text = models.TextField()
        slug = models.CharField(max_length=64, blank=True, default=False)
        pub_date = models.DateTimeField("publication date")
        image = models.FileField(blank=True, storage=image_storage)
        headline = models.BooleanField(default=False)
        approved = models.BooleanField(default=False)
        user = models.ForeignKey('User')

        def save(self, *args, **kwargs):

            self.slug = slugify(self.title)
            super(Article, self).save(*args, **kwargs)

        def __str__(self):
            return self.title
