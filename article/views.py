from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext, loader
# Create your views here.

from .models import Article
from .models import User

from .forms import CadastroForm

def index(request):

    #lista 5 ultimas noticias  inseridas
    article_list = Article.objects.order_by('-pub_date')[:5]
    clans = User.objects.all

    return render(request, 'article/article_list2.html', { 'article_list' : article_list, 'clans' : clans })
    #return HttpResponse(tpl.render(context))


def read_article(request, article_id=1, slug=''):
        if not article_id:
            article = Article.objects.get(slug=slug)
        else:
            article = Article.objects.get(pk=article_id)

        if not article:
            return Http404("Artigo Inexistente")


        return render(request, 'article/article_detailed.html', {'article':article, 'title' : article.title  })



def user_signup(request):

    if request.method == 'POST':
        form = CadastroForm(request.POST)

        if form.is_valid():
            return HttpResponseRedirect('/cadastro/sucesso/')
        else:
            HttpResponse('Erro')

    else:
        form = CadastroForm()

    return render(request, 'article/signup/new_user.html', {'form': form})



def user_signup_sucess(request = None):

    nome = request.POST.get('name')

    return HttpResponse(nome)

def profile(request,username):
    pass
