from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'read/(?P<article_id>\d+)', views.read_article, name='read'),
    url(r'read/(?P<article_id>\d+)/(?P<article_name>)', views.read_article, name='read'),
    url(r'read/(?P<slug>)', views.read_article, name='read'),

]
