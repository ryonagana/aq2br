from django import forms

from .models import SEX_CHOICE, MASCULINE
from file.models import Game

class CadastroForm(forms.Form):
    name = forms.CharField(label="Nome", max_length=65)
    surname = forms.CharField(label="Sobrenome", max_length=127)
    username = forms.CharField(label="Nome de Usuário", max_length=20)
    email = forms.EmailField(label="E-mail", max_length=127)
    clan_tag = forms.CharField(label="Tag de Clan", max_length=6)
    favourite_game = forms.ModelChoiceField(widget=forms.Select, queryset=Game.objects.all(), to_field_name="id", empty_label="Nenhum",  label="Jogo Favorito")
    passwd = forms.CharField(widget=forms.PasswordInput, label="Senha", max_length=25)
    confirm_passwd = forms.CharField(widget=forms.PasswordInput, label="Repita Senha", max_length=25)
    sex = forms.ChoiceField(label="Sexo", choices=SEX_CHOICE)

    def clean(self):


        '''
        faz a verificacao do formulario de comnfirmação de senha

        '''
        passwd = self.cleaned_data.get('passwd')
        conf   = self.cleaned_data.get('confirm_passwd')

        if passwd and passwd != conf:
            raise forms.ValidationError('As senha nao são iguais. por favor tente novamente')

        if len(passwd) < 6:
             raise forms.ValidationError('as senhas devem ter no minimo 6 caracteres')

        return self.cleaned_data



class LoginForm(forms.Form):
    username = forms.CharField(max_length=128, label='Nome de Usuário')
    passwd   =     passwd = forms.CharField(widget=forms.PasswordInput)