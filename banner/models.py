from django.db import models
from django.template.defaultfilters import slugify
# Create your models here.


class Banner(models.Model):
    name = models.CharField(max_length=30)
    slug = models.CharField(max_length=30, blank=True)
    description = models.TextField(blank=True)
    approved = models.BooleanField(blank=True, default=False)
    article = models.ForeignKey('article.Article')
    start = models.DateTimeField()
    end = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Banner, self).save(*args, **kwargs)

    def __str__(self):
        return self.name