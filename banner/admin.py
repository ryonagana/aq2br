from django.contrib import admin

# Register your models here.
from .models import Banner

class BannerAdmin(admin.ModelAdmin):
    pass




admin.site.register(Banner, BannerAdmin)