from django import template

from banner.models import Banner


register = template.Library()

@register.inclusion_tag('tpl_shared/carousel.html')
def main_banner():
    banner = Banner.objects.filter(approved=True)
    return {'banner' : banner}