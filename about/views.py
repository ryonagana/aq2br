from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext, loader

# Create your views here.
from .  import  models
import article
from article.models import User

def index(request):
    players = User.objects.all()[:20]

    return render(request, 'about/about.html', {'players' : players})