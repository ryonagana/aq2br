from django.core.files.storage import Storage
from django.conf import settings

class FileStorage (Storage):
    def __init__(self, option = None):
            if not option:
                option = settings.CUSTOM_STORAGE_OPTIONS


class ImageStorage (Storage):
    def __init__(self, option = None):
        if not option:
            option = settings.CUSTOM_STORAGE_OPTIONS
