from django.db import models
from django.template.defaultfilters import slugify
from django.core.files.storage import default_storage



import os

GAME_AQ2='AQ2'
GAME_Q2TP = 'Q2'
GAME_OTHERS = 'OT'

LIST_GAMES=[
    (GAME_AQ2, 'Action Quake 2'),
    (GAME_Q2TP, 'Quake 2'),
    (GAME_OTHERS, 'Outros')
]


class Game (models.Model):
    name = models.CharField(max_length=16)
    approved = models.BooleanField(default=False)
    slug = models.CharField(max_length=16, null=True, blank=True)


    def label_from_instance(self, obj):
        return "{0}".format(self.name)


    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Game, self).save(*args, **kwargs)


class Category (models.Model):
    name = models.CharField(max_length=30)
    imagepath = models.FileField(null=True, blank=True)
    active = models.NullBooleanField(default=False)
    slug = models.CharField(max_length=30, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)




class File(models.Model):
    name = models.CharField(max_length=60)
    filename = models.FileField(storage=default_storage)
    approved = models.BooleanField(default=False)
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey('Category',   on_delete=models.CASCADE)
    user = models.ForeignKey('article.User', on_delete=models.CASCADE)
    game = models.ForeignKey('Game', null=True)
    slug  = models.SlugField(blank=True, null=True)
    download_counter = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.filename)
        super(File, self).save(*args, **kwargs)


    def fname(self):
        return os.path.basename(self.filename.name)
