from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound, Http404
from django.template import RequestContext, loader
# Create your views here.
from django.template.defaultfilters import slugify


from django.core.files.storage import default_storage

from . import models
from mainnav.models import MainNav

import os

def index(request):
    tpl = loader.get_template('file/games.html')
    list = models.Game.objects.filter(approved=True)

    context  = RequestContext(request, {

        'games' : list
    })

    return render(request, 'file/games.html', { 'games' : list })
    #return HttpResponse(tpl.render(context))


def show_game_data(request, slug):
    game_chosen = models.Game.objects.filter(slug=slug)

    if not game_chosen:
        raise Http404('Pagina Não Encontrada')

    categories = models.Category.objects.filter(active=True)
    navbar = MainNav.objects.filter(slug=slug).all


    return render(request,'file/cat_list.html', {
            'category_list': categories,
            'game': game_chosen,
            'navbar' : navbar,
            'slug' : slug,
        })



def download_section(request,cat_id, gid, name='default-file'):

    files = models.File.objects.filter(category__id=cat_id, game__id=gid)
    game = models.Game.objects.get(id=gid)
    category = models.Category.objects.get(id=cat_id)


    return render(request, 'file/show_files.html', { 'file_list' : files, 'game' : game, 'category' : category })
    #return HttpResponse(template.render(context))


def download_section_slugs(request, game_slug, category):

    if not category or not game_slug:
        return HttpResponseRedirect('downloads')


    try:
        category_obj = models.Category.objects.get(slug=category)
        game_obj = models.Game.objects.get(slug=game_slug)
        files_obj = models.File.objects.filter(category__id=category_obj.id, game__id=game_obj.id)
    except Exception as ex:
        raise Http404('Categoria Inexistente')
    return render(request, 'file/show_files.html', {'file_list': files_obj, 'game': game_obj, 'category': category_obj})






def acquire_file(request, id):
   f = models.File.objects.get(pk=id)
   path = f.fname()

   if not default_storage.exists(f.filename):
        return Http404('Desculpe o arquivo que estar tentando pegar nao existe. por favor tente mais tarde')




   file_data = default_storage.open(f.filename).read()

   f.download_counter += 1
   f.save()
   extension =  f.filename.name.replace('./','').split('.') #limpa o nome do arquivo
   response = HttpResponse(file_data, content_type='application/octet-stream')
   response['Content-Disposition'] = 'attachment; filename={0}.{1}'.format(slugify(extension[0]), extension[1])
   response['Content-Length'] = default_storage.path(f.filename)

   return response
