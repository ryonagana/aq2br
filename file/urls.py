from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'game/(?P<slug>[\w-]+)$', views.show_game_data, name='game'),
    url(r'file/(?P<game_slug>[\w-]+)/(?P<category>[\w-]+)/$', views.download_section_slugs, name='file' ),
    url(r'get/(?P<id>\d+)', view=views.acquire_file, name='get'),
]
