from django.contrib import admin

# Register your models here.
from .models import Category, File, Game

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'active')


class FileAdmin(admin.ModelAdmin):
    pass


class GameAdmin(admin.ModelAdmin):
    pass

admin.site.register(Category, CategoryAdmin)
admin.site.register(File, FileAdmin)
admin.site.register(Game, GameAdmin)