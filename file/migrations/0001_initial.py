# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
                ('filename', models.CharField(max_length=60)),
                ('approved', models.BooleanField(default=False)),
                ('slug', models.SlugField()),
                ('download_counter', models.IntegerField()),
                ('category', models.ForeignKey(to='file.Category')),
                ('user', models.ForeignKey(to='article.User')),
            ],
        ),
    ]
