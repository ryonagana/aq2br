# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0003_category_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='download_counter',
            field=models.IntegerField(default=0),
        ),
    ]
