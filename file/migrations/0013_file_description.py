# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-03 13:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0012_category_imagepath'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
    ]
