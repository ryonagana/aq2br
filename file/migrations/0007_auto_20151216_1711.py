# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0006_auto_20151216_1643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='filename',
            field=models.FileField(upload_to=''),
        ),
    ]
