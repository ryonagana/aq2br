# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0005_auto_20151216_1640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
    ]
