# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0007_auto_20151216_1711'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='game',
            field=models.CharField(default='AQ2', choices=[('AQ2', 'Action Quake 2'), ('Q2', 'Quake 2'), ('OT', 'Outros')], null=True, max_length=3),
        ),
    ]
