# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0011_auto_20151221_1232'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='imagepath',
            field=models.FileField(null=True, blank=True, upload_to=''),
        ),
    ]
