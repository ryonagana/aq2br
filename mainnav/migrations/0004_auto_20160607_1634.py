# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-07 19:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainnav', '0003_auto_20160607_1626'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mainnav',
            name='item',
        ),
        migrations.AddField(
            model_name='mainnav',
            name='item',
            field=models.ManyToManyField(blank=True, default=False, to='mainnav.MenuItem'),
        ),
    ]
