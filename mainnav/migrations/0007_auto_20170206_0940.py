# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-06 11:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainnav', '0006_auto_20170203_1709'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mainnav',
            name='slug',
            field=models.CharField(blank=True, max_length=40),
        ),
    ]
