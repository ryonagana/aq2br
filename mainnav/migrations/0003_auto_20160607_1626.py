# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-07 19:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainnav', '0002_mainnav_game'),
    ]

    operations = [
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40)),
                ('slug', models.CharField(max_length=40)),
            ],
        ),
        migrations.AddField(
            model_name='mainnav',
            name='item',
            field=models.ForeignKey(blank=True, default=False, on_delete=django.db.models.deletion.CASCADE, to='mainnav.MenuItem'),
        ),
    ]
