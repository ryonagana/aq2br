from django.db import models
from django.template.defaultfilters import slugify

import file.models as Files


# Create your models here.
class MainNav(models.Model):
    title = models.CharField(max_length=40)
    slug = models.CharField(max_length=40, blank=True)
    approved = models.BooleanField(default=False)
    priority = models.IntegerField(default=1, blank=True)
    game = models.ForeignKey(Files.Game, on_delete=None, blank=True, null=True)
    item = models.ManyToManyField("MenuItem", blank=True)



    def save(self, *args, **kwargs ):
        self.slug = slugify(self.title)
        super(MainNav, self).save(*args, **kwargs)

    def __str__(self):
        return "{0}".format(self.title)

    def __repr__(self):
        return "{0}".format(self.title)




class MenuItem(models.Model):
    title = models.CharField(max_length=40)
    slug = models.CharField(max_length=40, blank=True)

    def save(self, *args, **kwargs ):
        self.slug = slugify(self.title)
        super(MenuItem, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


