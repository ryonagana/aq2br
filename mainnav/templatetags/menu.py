from django import template
from mainnav.models import MainNav, MenuItem
from file.models import Game


register = template.Library()

@register.inclusion_tag('top/top_width.html')
def render_menu(menu):
    menu = MainNav.objects.filter(approved=True).order_by('priority')
    return {'menu' : menu}


@register.inclusion_tag('tpl_shared/game_bar.html')
def section(value, slug):
    menu = MainNav.objects.get(slug=slug)
    return {'menu':menu.item.all(), 'slug': slug, 'game_name' : menu.title }

    pass



register.filter('menu', render_menu)
register.filter('game_subsection', section )
