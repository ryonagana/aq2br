# Register your models here.
from django.contrib import admin
from .models import MainNav, MenuItem


class MainNavAdmin(admin.ModelAdmin):
    fields = ('title', 'priority',  'slug','game', 'item', 'approved')


class MenuItemAdmin(admin.ModelAdmin):
    pass



admin.site.register(MainNav, MainNavAdmin)
admin.site.register(MenuItem, MenuItemAdmin)
